# Proyecto_ELO329_2022

El proyecto trata sobre el modelamiento y manejo de un aeropuerto en donde principalmente nos enfocamos en visualizar los vuelos que contiene este aeropuerto, 
además se incluye las pistas, compañías, y los parámetros del vuelo como por ejemplo el origen, destino, y la hora. \
En la pantalla principal se podría visualizar lo descrito anteriormente, además se añadieron 3 métodos de búsqueda, que son búsqueda por Código de vuelo, búsqueda por
compañía y búsqueda por lugar de destino.


# Integrantes
Matias Ayala Nanjari \
Ariel Bravo Ponce  \
Sergio Rojas Escanilla  \
Ignacio Araya Salinas  

# Java version
Tarea realizada con la siguiente version de java: \
java 18.0.1 2022-04-19 \
Java(TM) SE Runtime Environment (build 18.0.1+10-24) \
Java HotSpot(TM) 64-Bit Server VM (build 18.0.1+10-24, mixed mode, sharing) \
javac 18.0.1 \
javafx 18.0.1 

# Compilación
1.-Agregamos la libreria de javafx, vamos a file-> project structure->libraries -> "+", java-> y seleccionamos el path donde esta la carpeta lib de javafx. \
2.-Agregar las VM options vamos a run-> edit configurations -> modify options -> add VM options-> agregamos el path donde esta javafx \
	--module-path *path to javafx*\lib --add-modules javafx.controls,javafx.media,javafx.fxml \
3.verificar SDK file-> project structure \
4.verificar project language level file-> project structure (mayor a 7, 16 utilizado) \

5. Se puede ejcutar normalmente los programas 

# Link video
https://youtu.be/7tyEHnFC4NM

## Add your files

- [ ] [Create](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#create-a-file) or [upload](https://docs.gitlab.com/ee/user/project/repository/web_editor.html#upload-a-file) files
- [ ] [Add files using the command line](https://docs.gitlab.com/ee/gitlab-basics/add-file.html#add-a-file-using-the-command-line) or push an existing Git repository with the following command:

```
cd existing_repo
git remote add origin https://gitlab.com/matias.ayala1/proyecto_elo329_2022.git
git branch -M main
git push -uf origin main
```

