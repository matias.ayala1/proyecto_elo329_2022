package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.geometry.Pos;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.Calendar;

public class MainFX extends Application {


    public Button btnBuscarVuelo;
    public Button btnBuscarCompania;
    public ArrayList<Aeropuerto> aeropuertos = new ArrayList<>();
    public Aeropuerto aeropuerto = new Aeropuerto();

    @Override
    public void start(Stage primaryStage) throws Exception{
        //CON XML

        /*
        Parent root = FXMLLoader.load(getClass().getResource("sample2.fxml"));
        primaryStage.setTitle("Aeropuerto los pibardos");
        Scene scene = new Scene(root, 702,614);
        primaryStage.setScene(scene);

        Aeropuerto aeropuerto1 = new Aeropuerto("Aeropuerto los pibardos", "Chile", 10000);
        AvionComercial avionComercial = new AvionComercial(1000, 10000, 20);
        //Pasajeros
        Pasajero pasajero1 = new Pasajero("Mati", "12345", "Chilena", 22,2, "LA500");
        Pasajero pasajero2 = new Pasajero("Ariel", "12345", "Chilena", 22,2, "LA500");

        //Trabajadores
        Trabajador trabajador1 = new Trabajador("Checho", "12345", "Chilena", 22, 5000000, Cargo.AEROMOZO);

        //Vuelos
        Calendar calendar = Calendar.getInstance();


        Vuelo vuelo1 = new Vuelo("LA500", avionComercial, "Santiago", "Akihabara", calendar.getTime(),5 );
        vuelo1.addPasajeros(pasajero1);
        vuelo1.addPasajeros(pasajero2);

        Vuelo vuelo2 = new Vuelo("AC395", avionComercial, "Santiago", "Canada", calendar.getTime(),5);
        Vuelo vuelo3 = new Vuelo("AE456", avionComercial, "Santiago", "New York", calendar.getTime(),6);
        Vuelo vuelo4 = new Vuelo("LA786", avionComercial, "Santiago", "Londres", calendar.getTime(),6);
        Vuelo vuelo5 = new Vuelo("LA777", avionComercial, "Santiago", "Copenhagen", calendar.getTime(),6);
        Vuelo vuelo6 = new Vuelo("LA986", avionComercial, "Santiago", "Estocolmo", calendar.getTime(),5);
        //Compañias
        Compania compania1 = new Compania("LATAM", "12345");
        Compania compania2 = new Compania("American Airlines", "234823");
        Compania compania3 = new Compania("Air Canada", "13842");
        compania1.addVuelo(vuelo1);
        compania1.addTrabajador(trabajador1);

        compania2.addVuelo(vuelo2);
        compania3.addVuelo(vuelo3);

        compania1.addVuelo(vuelo4);
        compania1.addVuelo(vuelo5);
        compania1.addVuelo(vuelo6);


        //Pistas
        Pista pista1 = new Pista(5, 1000);
        pista1.addCompanias(compania1);
        pista1.addCompanias(compania2);
        pista1.addCompanias(compania3);
        Pista pista2 = new Pista(6, 1000);
        pista2.addCompanias(compania1);


        //Aeropuerto
        aeropuerto1.addPistas(pista1);
        aeropuerto1.addPistas(pista2);

        primaryStage.show();
        addData();
          */

        //SIN XML

        Pane pane = new Pane();
        pane.setPrefWidth(890);
        pane.setPrefHeight(740);

        Button btnVuelo = new Button("Buscar vuelo");
        btnVuelo.setLayoutX(193);
        btnVuelo.setLayoutY(40);
        Button btnCompania = new Button("Buscar compañia");
        btnCompania.setLayoutX(440);
        btnCompania.setLayoutY(40);
        Button btnRestablecer = new Button("Restablecer");
        btnRestablecer.setLayoutX(728);
        btnRestablecer.setLayoutY(9);
        Button btnDestino = new Button("Buscar destino");
        btnDestino.setLayoutX(721);
        btnDestino.setLayoutY(40);


        TextField textField = new TextField();
        textField.setLayoutX(25);
        textField.setLayoutY(40);

        TextField textField1 = new TextField();
        textField1.setLayoutX(288);
        textField1.setLayoutY(40);

        TextField textField2 = new TextField();
        textField2.setLayoutX(562);
        textField2.setLayoutY(40);

        pane.getChildren().addAll(textField,textField1,textField2,btnVuelo,btnCompania,btnRestablecer,btnDestino);







        TextArea textArea = new TextArea();
        textArea.setPrefWidth(880);
        textArea.setPrefHeight(615);
        textArea.setLayoutX(9);
        textArea.setLayoutY(94);
        pane.getChildren().add(textArea);

        addData();
        textArea.setText(aeropuertos.get(0).mostrarData());

        btnVuelo.setOnAction(e-> {
            textArea.setText(aeropuertos.get(0).busquedaPorVuelo(textField.getText()));
            textField.clear();
        });


        btnCompania.setOnAction(e->{
                    textArea.setText(aeropuertos.get(0).getCompania(textField1.getText()));
                    textField1.clear();
                });

        btnDestino.setOnAction(e->{
            textArea.setText(aeropuertos.get(0).busquedaDestino(textField2.getText()));
            textField2.clear();
        });


        btnRestablecer.setOnAction(e-> textArea.setText(aeropuertos.get(0).mostrarData()));



        Scene scene = new Scene(pane, 890, 740);
        primaryStage.setTitle("Aeropuerto ELO329");
        primaryStage.setScene(scene);
        primaryStage.show();



    }

    public static void main(String[] args) {
        launch(args);

    }

    public void addData(){
        //Creacion del aeropuerto
        Aeropuerto aeropuerto1 = new Aeropuerto("Aeropuerto ELO329", "Chile", 10000);

        AvionComercial avionComercial = new AvionComercial(1000, 10000, 20);
        //Pasajeros
        Pasajero pasajero1 = new Pasajero("Mati", "12345", "Chilena", 22,2, "LA500");
        Pasajero pasajero2 = new Pasajero("Ariel", "12345", "Chilena", 22,2, "LA500");

        //Trabajadores
        Trabajador trabajador1 = new Trabajador("Checho", "12345", "Chilena", 22, 5000000, Cargo.AEROMOZO);

        //Vuelos
        Calendar calendar = Calendar.getInstance();


        Vuelo vuelo1 = new Vuelo("LA500", avionComercial, "Santiago", "Akihabara", calendar.getTime(),5 );
        vuelo1.addPasajeros(pasajero1);
        vuelo1.addPasajeros(pasajero2);

        Vuelo vuelo2 = new Vuelo("AC395", avionComercial, "Santiago", "Toronto", calendar.getTime(),5);
        Vuelo vuelo3 = new Vuelo("AE456", avionComercial, "Santiago", "New York", calendar.getTime(),6);
        Vuelo vuelo4 = new Vuelo("LA786", avionComercial, "Santiago", "Londres", calendar.getTime(),6);
        Vuelo vuelo5 = new Vuelo("LA777", avionComercial, "Santiago", "Copenhagen", calendar.getTime(),6);
        Vuelo vuelo6 = new Vuelo("LA986", avionComercial, "Santiago", "Estocolmo", calendar.getTime(),5);
        Vuelo vuelo7 = new Vuelo("AC968", avionComercial, "Santiago", "Toronto", calendar.getTime(),5);
        Vuelo vuelo8 = new Vuelo("AC450", avionComercial, "Santiago", "Paris", calendar.getTime(),5);
        Vuelo vuelo9 = new Vuelo("AC690", avionComercial, "Santiago", "Madrid", calendar.getTime(),5);
        Vuelo vuelo10 = new Vuelo("TEL342", avionComercial, "Santiago", "Madrid", calendar.getTime(),7);
        Vuelo vuelo11 = new Vuelo("TEL252", avionComercial, "Santiago", "Miami", calendar.getTime(),7);
        Vuelo vuelo12 = new Vuelo("TEL354", avionComercial, "Santiago", "Cancun", calendar.getTime(),7);



        //Compañias
        Compania compania1 = new Compania("LATAM", "12345");
        Compania compania2 = new Compania("American Airlines", "234823");
        Compania compania3 = new Compania("Air Canada", "13842");
        Compania compania4 = new Compania("TELEMATICA", "2019305");

        compania1.addVuelo(vuelo1);
        compania1.addTrabajador(trabajador1);
        compania1.addVuelo(vuelo4);
        compania1.addVuelo(vuelo5);
        compania1.addVuelo(vuelo6);

        compania2.addVuelo(vuelo2);

        compania3.addVuelo(vuelo3);
        compania3.addVuelo(vuelo7);
        compania3.addVuelo(vuelo8);
        compania3.addVuelo(vuelo9);

        compania4.addVuelo(vuelo10);
        compania4.addVuelo(vuelo11);
        compania4.addVuelo(vuelo12);






        //Pistas
        Pista pista1 = new Pista(5, 1000);
        pista1.addCompanias(compania1);
        pista1.addCompanias(compania2);
        pista1.addCompanias(compania3);
        Pista pista2 = new Pista(6, 1000);
        pista2.addCompanias(compania1);
        pista2.addCompanias(compania2);
        pista2.addCompanias(compania3);

        Pista pista3 = new Pista(7, 1000);
        pista3.addCompanias(compania4);


        //Aeropuerto
        aeropuerto1.addPistas(pista1);
        aeropuerto1.addPistas(pista2);
        aeropuerto1.addPistas(pista3);
        aeropuertos.add(aeropuerto1);

    }

/*
    public void buscarVuelo(ActionEvent event) {
        System.out.println(aeropuerto.getPais());
    }

    public void buscarCompania(ActionEvent event) {
    }
*/

}
