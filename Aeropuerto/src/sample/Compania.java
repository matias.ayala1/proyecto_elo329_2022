package sample;

import java.util.ArrayList;

public class Compania {
    public Compania(String nombre, String rutCompania) {
        this.nombre = nombre;
        this.rutCompania = rutCompania;
        vuelos = new ArrayList<Vuelo>();
        trabajadores = new ArrayList<Persona>();
    }
    public String mostarVuelo(){
        String data = "";
        for (Vuelo v: vuelos){
            data += this+ v.toString() + "\n" ;

        }
        return data;
    }

    public String mostarVuelo(int pista){
        String data = "";
        for (Vuelo v: vuelos){
            if (v.getPista() == pista){
                data += this + v.toString(pista) + "\n" ;
            }
        }
        return data;
    }

    public Vuelo mostarVuelo(String vuelo){
        Vuelo vuelo1 = null;
        for (Vuelo v: vuelos){
            if (v.getCodVuelo().equals(vuelo)){
                vuelo1 = v;
            }
        }
        return vuelo1;
    }
    public Vuelo mostarVueloPista(int pista){
        Vuelo vuelo1 = null;
        for (Vuelo v: vuelos){
            if (v.getPista() == pista){
                vuelo1 = v;
            }
        }
        return vuelo1;
    }

    public String mostrarDestinoVuelo(String destino, int pista){
        String data = "";
        for (Vuelo v: vuelos){
            if (v.getDestino().equals(destino) && v.getPista() == pista){
                data += this + v.toStringDestino(destino) + "\n" ;
            }
        }
        return data;
    }


    @Override
    public String toString() {
        return "Compania: " + nombre + '\'' +
                " | ";
    }

    public void addVuelo(Vuelo vuelo){
        vuelos.add(vuelo);
    }
    public void addTrabajador(Trabajador trabajador){
        trabajadores.add(trabajador);
    }

    public String getNombre() {
        return nombre;
    }

    public String getRutCompania() {
        return rutCompania;
    }

    public ArrayList<Vuelo> getVuelos() {
        return vuelos;
    }
    public boolean checkVuelo(String vuelo){
        boolean b = false;
        for (Vuelo v : vuelos){
            if(v.getCodVuelo().equals(vuelo)){
                b = true;
            }
        }
        return b;
    }

    public ArrayList<Persona> getTrabajadores() {
        return trabajadores;
    }



    private final String nombre;
    private final String rutCompania;
    private ArrayList<Vuelo> vuelos;
    private ArrayList<Persona> trabajadores;
}