package sample;

import java.util.ArrayList;

public class Pista {

    public Pista(int numPista,double distancia) {
        this.numPista = numPista;
        this.distancia = distancia;
        companias = new ArrayList<Compania>();
    }

    public int getNumPista() {
        return numPista;
    }

    @Override
    public String toString() {
        return "Pista: " +
                numPista +
                " | ";
    }

    public String mostrarCompanias() {
        String data = "";
        for (Compania c : companias){
            data += c.mostarVuelo(numPista);
        }

        return data;
    }

    public String mostrarDestino(String destino) {
        String data = "";
        for (Compania c : companias){
            data += c.mostrarDestinoVuelo(destino, numPista);
        }

        return data;
    }


    public void addCompanias(Compania compania){
        companias.add(compania);
    }

    public double getDistancia() {
        return distancia;
    }

    public boolean verificaCompañia(String compa){
        boolean b = false;
        for (Compania c : companias){
            if (c.getNombre().equals(compa)){
                b = true;
            }
        }
        return b;
    }

    public String getCompaniaData(String compa){
        String s = "";
        for (Compania c : companias){
            if(c.getNombre().equals(compa)){
                s += c.mostarVuelo(numPista);
            }
        }
        return s;
    }
    public boolean checkVuelo(String vuelo){
        boolean b = false;
        for (Compania c : companias){
            if(c.checkVuelo(vuelo)){
                b = true;
            }
        }
        return b;
    }


    public Vuelo getVueloData(String vuelo){
        Vuelo v = null;
        for (Compania c : companias){
            if(c.checkVuelo(vuelo)){
                v = c.mostarVuelo(vuelo);

            }
        }
        return v;
    }

    int numPista;
    private ArrayList<Compania> companias;
    private double distancia;
}
