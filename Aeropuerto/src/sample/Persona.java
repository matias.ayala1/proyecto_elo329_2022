package sample;

public class Persona {
    public Persona(String nombre, String rut, String nacionalidad, int edad){
        this.nombre=nombre;
        this.rut=rut;
        this.nacionalidad=nacionalidad;
        this.edad=edad;
    }

    public String getNombre() {
        return nombre;
    }

    public String getRut() {
        return rut;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public int getEdad() {
        return edad;
    }

    private final String nombre;
    private final String rut;
    private final String nacionalidad;
    private final int edad;
}
