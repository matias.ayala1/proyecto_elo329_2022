package sample;

public class Trabajador extends Persona {
    public Trabajador(String nombre, String rut, String nacionalidad, int edad, double sueldo, Cargo cargo) {
        super(nombre, rut, nacionalidad, edad);
        this.sueldo = sueldo;
        this.cargo = cargo;
    }

    private double sueldo;
    private Cargo cargo;

    public double getSueldo() {
        return sueldo;
    }

    public void setSueldo(double sueldo) {
        this.sueldo = sueldo;
    }

    public Cargo getCargo() {
        return cargo;
    }

    public void setCargo(Cargo cargo) {
        this.cargo = cargo;
    }
}
