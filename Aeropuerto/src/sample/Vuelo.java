package sample;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class Vuelo {
    public Vuelo(String codVuelo, Avion avion, String origen, String destino , Date date, int pista){
        this.codVuelo = codVuelo;
        this.avion=avion;
        this.origen=origen;
        this.destino=destino;
        pasajeros = new ArrayList<Persona>();
        this.date = date;
        this.pista = pista;

    }

    public String mostarPasajeros() {
        String data = "";
        for (Persona p : pasajeros){
            Pasajero pasajero = (Pasajero) p;
            data += pasajero.toString();
            data += "\n";
        }
        return data;
    }

    @Override
    public String toString() {
        return "Pista: " + pista + '\'' +
                "Vuelo: " +
                "codVuelo='" + codVuelo + '\'' +
                ", origen='" + origen + '\'' +
                ", destino='" + destino + '\'' +
                ", Fecha='" + date.toString() + '\'' +
                " | ";
    }

    public String toString(int pista) {
        String data = "";
        if (this.pista == pista){
            data += "Pista: " + pista + '\'' +
                    ", Vuelo: " +
                    "codVuelo='" + codVuelo + '\'' +
                    ", origen='" + origen + '\'' +
                    ", destino='" + destino + '\'' +
                    ", Fecha='" + date.toString() + '\'' +
                    " | ";
        }
        return data;
    }

    public String toStringDestino(String destino) {
        String data = "";
        if (this.destino.equals(destino)){
            data += "Pista: " + pista + '\'' +
                    ", Vuelo: " +
                    "codVuelo='" + codVuelo + '\'' +
                    ", origen='" + origen + '\'' +
                    ", destino='" + destino + '\'' +
                    ", Fecha='" + date.toString() + '\'' +
                    " | ";
        }
        return data;
    }

    public void addPasajeros(Pasajero persona){
        pasajeros.add(persona);
    }

    public String getCodVuelo() {
        return codVuelo;
    }

    public Avion getAvion() {
        return avion;
    }

    public String getOrigen() {
        return origen;
    }

    public String getDestino() {
        return destino;
    }
    public int getPista(){
        return pista;
    }

    private ArrayList<Persona> pasajeros;
    private String codVuelo;
    private Avion avion;
    private String origen;
    private String destino;
    private Date date;
    private int pista;

}
