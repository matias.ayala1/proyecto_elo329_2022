package sample;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.fxml.FXML;
import javafx.scene.control.TextField;
import javafx.stage.Stage;
import java.util.ArrayList;
import java.util.Calendar;


public class Aeropuerto {
    public Aeropuerto(String nombre, String pais, double area) {
        this.nombre = nombre;
        this.pais = pais;
        this.area = area;
        pistas = new ArrayList<Pista>();

    }

    public Aeropuerto() {

    }

    public String getNombre() {
        return nombre;
    }

    public String mostrarData() {
        String data = "";
        for (Pista p : pistas){
            data += p.mostrarCompanias();
        }
        return data;
    }

    public void addPistas(Pista pista){
        pistas.add(pista);
    }


    public void getPistas(){
        for (Pista p : pistas){
            System.out.println(p);
        }
    }

    public String getPais() {
        return pais;
    }


    public double getArea() {
        return area;
    }

    public String getCompania(String compa){
        String data = "";
        for (Pista p : pistas){
            if (p.verificaCompañia(compa)){
                data += p.getCompaniaData(compa);
            }
        }
        return data;
    }

    public String busquedaPorVuelo(String vuelo){
        String data = "";
        for (Pista p : pistas){
            if(p.checkVuelo(vuelo)){
                Vuelo vuelo1 = p.getVueloData(vuelo);
                data +=  vuelo1;
                break;
            }
        }
        return data;
    }

    public String busquedaDestino(String destino){
        String data = "";
        for (Pista p : pistas){
            data += p.mostrarDestino(destino);
        }
        return data;
    }


    private String nombre;
    private ArrayList<Pista> pistas;
    private String pais;
    private double area;


}
