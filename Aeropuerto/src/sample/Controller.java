package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

import java.util.ArrayList;
import java.util.Calendar;

public class Controller {
    @FXML public Button btnGeneral;
    @FXML public Button btnVuelo;
    @FXML public Button btnCompania;
    @FXML public TextField textVuelo;
    @FXML public TextField textCompania;
    private Aeropuerto aeropuerto;
    public Button btnBuscarVuelo;
    public Button btnBuscarCompania;

    public Controller() {

    }


    public void buscarVuelo(ActionEvent event) {
        System.out.println(aeropuerto.getNombre());
    }

    public void buscarCompania(ActionEvent event) {
    }


    public void addData(){
        //Creacion del aeropuerto
        Aeropuerto aeropuerto1 = new Aeropuerto("Aeropuerto los pibardos", "Chile", 10000);
        AvionComercial avionComercial = new AvionComercial(1000, 10000, 20);
        //Pasajeros
        Pasajero pasajero1 = new Pasajero("Mati", "12345", "Chilena", 22,2, "LA500");
        Pasajero pasajero2 = new Pasajero("Ariel", "12345", "Chilena", 22,2, "LA500");

        //Trabajadores
        Trabajador trabajador1 = new Trabajador("Checho", "12345", "Chilena", 22, 5000000, Cargo.AEROMOZO);

        //Vuelos
        Calendar calendar = Calendar.getInstance();


        Vuelo vuelo1 = new Vuelo("LA500", avionComercial, "Santiago", "Akihabara", calendar.getTime(),5 );
        vuelo1.addPasajeros(pasajero1);
        vuelo1.addPasajeros(pasajero2);

        Vuelo vuelo2 = new Vuelo("AC395", avionComercial, "Santiago", "Canada", calendar.getTime(),5);
        Vuelo vuelo3 = new Vuelo("AE456", avionComercial, "Santiago", "New York", calendar.getTime(),6);
        Vuelo vuelo4 = new Vuelo("LA786", avionComercial, "Santiago", "Londres", calendar.getTime(),6);
        Vuelo vuelo5 = new Vuelo("LA777", avionComercial, "Santiago", "Copenhagen", calendar.getTime(),6);
        Vuelo vuelo6 = new Vuelo("LA986", avionComercial, "Santiago", "Estocolmo", calendar.getTime(),5);
        //Compañias
        Compania compania1 = new Compania("LATAM", "12345");
        Compania compania2 = new Compania("American Airlines", "234823");
        Compania compania3 = new Compania("Air Canada", "13842");
        compania1.addVuelo(vuelo1);
        compania1.addTrabajador(trabajador1);

        compania2.addVuelo(vuelo2);
        compania3.addVuelo(vuelo3);

        compania1.addVuelo(vuelo4);
        compania1.addVuelo(vuelo5);
        compania1.addVuelo(vuelo6);


        //Pistas
        Pista pista1 = new Pista(5, 1000);
        pista1.addCompanias(compania1);
        pista1.addCompanias(compania2);
        pista1.addCompanias(compania3);
        Pista pista2 = new Pista(6, 1000);
        pista2.addCompanias(compania1);


        //Aeropuerto
        aeropuerto1.addPistas(pista1);
        aeropuerto1.addPistas(pista2);
        this.aeropuerto = aeropuerto1;
    }



}


