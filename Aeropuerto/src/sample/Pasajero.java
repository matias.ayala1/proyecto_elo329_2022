package sample;

public class Pasajero extends Persona {
    public Pasajero(String nombre, String rut, String nacionalidad, int edad, int maleta, String vuelo) {
        super(nombre, rut, nacionalidad, edad);
        this.maleta=maleta;
        this.vuelo=vuelo;
    }
    //geter pasajeros
    public int getMaleta() {
        return maleta;
    }

    public String getVuelo() {
        return vuelo;
    }

    @Override
    public String toString() {
        return "Pasajero{" +
                "Nombre=" + super.getNombre() +
                ", maleta=" + maleta +
                ", vuelo='" + vuelo + '\'' +
                '}';
    }

    //Seter pasajeros
    public void setMaleta(int maleta) {
        this.maleta = maleta;
    }

    public void setVuelo(String vuelo) {
        this.vuelo = vuelo;
    }

    private int maleta;
    private String vuelo;
}
